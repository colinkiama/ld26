# Ludum Dare 26

This was going to be my entry for LD26. The theme was either 'minimalism' or
'minimalist'. It didn't end up turning out well.

100% of the code is Vala.

![screenshot](screenshot.png)

## Requirements

  * valac
  * libsdl1.2-dev
  * libsdl_image1.2-dev
  * libxml2-dev

On Ubuntu:

	sudo apt install libsdl-image1.2-dev libxml2-dev libvala-dev valac

## Compilation

	./compile.sh

## Running

	./bin/ld26
