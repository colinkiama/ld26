#!/bin/bash

# the packages for valac to load
PKGS="--pkg sdl --pkg sdl-image --pkg libxml-2.0" 

# where to put the binary; what to call it
OUTDIR=../bin
OUT=$OUTDIR/ld26

# all the vala files to compile
# set to compile all the vala files you find
SRC="*.vala Menu/*.vala"
# SRC="LD26.vala InputHandler.vala ImageManager.vala SDLRenderer.vala Point2D.vala SpriteSheet.vala Stage.vala GameStage.vala Menu.vala MenuButton.vala Rectangle2D.vala HelpStage.vala Level.vala Vector2D.vala Distance.vala Player.vala Bullet.vala PlayerVariables.vala MoneyStage.vala TiledLoader.vala"

# the options to pass to the C compiler
XOPTS="-X -lSDL_image -X -lm"

# go into src and compile with the 
cd src
mkdir -p $OUTDIR
valac $PKGS -o $OUT $SRC $XOPTS
