using SDL;

/**
 * A class with only static methods and fields which processes SDL event user
 * input and makes it available for use.
 */
public class InputHandler : Object {
	/** Whether the window's X button was pushed. **/
	public static bool hit_close = false;
	
	/** Whether the space is pressed. **/
	public static bool space_pressed = false;
	/** Whether the enter key is pressed **/
	public static bool enter_pressed = false;
	public static bool w_pressed = false;
	public static bool a_pressed = false;
	public static bool s_pressed = false;
	public static bool d_pressed = false;
	
	/** The current mouse location **/
	public static Point2D mouseSpot = null;
	/** Whether the left mouse button is pressed **/
	public static bool left_mouse_pressed = false;

	private static Event event; // the event struct which we will populate

	// shouldn't be able to instantiate this class
	private InputHandler () {}

	/**
	 * Initializes the handler. Must be called before any other methods in this
	 * class.
	 */
	public static void init () {
		mouseSpot = new Point2D ( { 0, 0 } );
	}

	/**
	 * Processes user input, so it can be made more easily accessible.
	 */
	public static void process_input () {
		while (Event.poll (out event) == 1) { // while there are still events
			switch (event.type) {
				case EventType.QUIT:
					hit_close = true;
					break;
				case EventType.MOUSEMOTION:
					mouseSpot.point.x = event.motion.x;
					mouseSpot.point.y = event.motion.y;
					break;
				case EventType.KEYDOWN:
					key_pressed ();
					break;
				case EventType.KEYUP:
					key_released ();
					break;
				case EventType.MOUSEBUTTONDOWN:
					mouse_button_pressed ();
					break;
				case EventType.MOUSEBUTTONUP:
					mouse_button_released ();
					break;
			}
		}
	}
	
	/** Deal with a key being pressed. **/
	private static void key_pressed () {
		switch (event.key.keysym.sym) {
			case KeySymbol.SPACE: space_pressed = true; break;
			case KeySymbol.RETURN: enter_pressed = true; break;
			case KeySymbol.w: w_pressed = true; break;
			case KeySymbol.a: a_pressed = true; break;
			case KeySymbol.s: s_pressed = true; break;
			case KeySymbol.d: d_pressed = true; break;
		}
	}
	
	/** Deal with a key being released. **/
	private static void key_released () {
		switch (event.key.keysym.sym) {
			case KeySymbol.SPACE: space_pressed = false; break;
			case KeySymbol.RETURN: enter_pressed = false; break;
			case KeySymbol.w: w_pressed = false; break;
			case KeySymbol.a: a_pressed = false; break;
			case KeySymbol.s: s_pressed = false; break;
			case KeySymbol.d: d_pressed = false; break;
		}
	}
	
	/** Deal with the mouse button being pressed **/
	private static void mouse_button_pressed () {
		switch (event.button.button) {
			case MouseButton.LEFT: left_mouse_pressed = true; break;
		}
	}
	
	/** Deal with the mouse button being released **/
	private static void mouse_button_released () {
		switch (event.button.button) {
			case MouseButton.LEFT: left_mouse_pressed = false; break;
		}
	}
}
