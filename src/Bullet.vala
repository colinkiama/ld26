using GLib.Math;

/**
 * A bullet shot by the player.
 */
public class Bullet : Point2D {
	private Vector2D motion; // how far the bullet travels each tick
	private int life; // life left in ticks
	
	/**
	 * Create a bullet at a point going in a certain direction with an amount of
	 * life.
	 *
	 * @param p The point representing the center of the bullet.
	 * @param dir The direction, in radians, the bullet is heading in.
	 * @param speed The speen in pixels at which the bullet is travelling.
	 * @param v The vector representing the direction the bullet is heading in.
	 * @param lif The amount of life left on the bullet.
	 */
	public Bullet (Point p, float dir, float speed, int lif) {
		base (p);
		
		float movex = cosf (dir) * speed;
		float movey = sinf (dir) * speed;
		motion = new Vector2D ({movex, movey});
		
		life = lif;
	}
	
	/**
	 * Moves the bullet along it's path if it is still "live".
	 */
	public void move () {
		if (life > 0) {
			life --;
		
			this.point.x += motion.get_x ();
			this.point.y += motion.get_y ();
		}
	}
	
	/**
	 * Returns whether the bullet is "dead".
	 *
	 * @return true if dead, false otherwise.
	 */
	public bool is_dead () {
		return life <= 0;
	}
}
