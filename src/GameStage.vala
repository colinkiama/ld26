using SDL; // not needed, but just in case
using GLib.Math;
using TiledLoader;

/**
 * Takes care of updating and rendering associated with the actual gameplay
 * aspect of the game.
 */
public class GameStage : Stage {
	private Player player = new Player ({ 64, 64 }); // the player
	private Level level = null; // the current level
	private Vector2D vec = null; // the vector between the center of the screen and the mouse
	
	private SList<Bullet> bulletList = new SList<Bullet> ();
	
	/**
	 * Create the menu screen given the game who created it.
	 *
	 * @param game The game who made this stage.
	 */
	public GameStage (LD26 game) {
		base (game);
		fillColor = game.screen.format.map_rgb (255, 255, 255);
		vec = new Vector2D ({ 320 - InputHandler.mouseSpot.get_x () , 240 - InputHandler.mouseSpot.get_y () });
		//level = new Level.from_file ("test.txt");
		level = TiledLoader.load_tiled ("another-test.tmx");
	}
	
	// overriden update method
	public override void update () {
		// check window closed
		if (InputHandler.hit_close) parent.quit ();
		// check for screenshot
		if (InputHandler.space_pressed) {
			parent.request_screenshot ();
			InputHandler.space_pressed = false;
		}
		// check for debug key
		if (InputHandler.enter_pressed) {
			InputHandler.enter_pressed = false;
			parent.change_stage (parent.money);
		}
		
		// update movement vector
		vec.point.x = 320 - InputHandler.mouseSpot.get_x ();
		vec.point.y = 240 - InputHandler.mouseSpot.get_y ();
		
		// shot if pressing left mouse, move otherwise
		if (InputHandler.left_mouse_pressed) {
			bulletList.append (new Bullet (player.point, vec.direction(), 4, 100));
		} else {
			player.move (vec);
		}
		
		// check collision of player with wall
		foreach (Rectangle2D wall in level.wallList) {
			if (player.mask.intersects_rectangle (wall.rectangle)) {
				player.goto_previous_spot ();
				break;
			}
		}
		
		Bullet[] deadBulList = {}; // resizeable array
		
		// move bullets
		foreach (Bullet bul in bulletList) {
			bul.move ();
			
			if (bul.is_dead ()) deadBulList += bul; // if dead add to list for deletion
			
			// check collision with wall
			foreach (Rectangle2D wall in level.wallList) {
				if (bul.is_inside_rectangle (wall.rectangle) && bul.is_dead () == false) {
					deadBulList += bul;
				}
			}
		}
		
		// remove dead bullets
		foreach (Bullet bul in deadBulList) {
			bulletList.remove (bul);
		}
		
		// recenter the view because the player has probably moved
		level.center_view (player.point);
	}
	
	// overriden render method
	public override void render () {
		level.draw ();
		Point playerPoint = { player.get_x () - level.view.x , player.get_y () - level.view.y };
		SDLRenderer.draw_image (ImageManager.circle, playerPoint, true);
		
		foreach (Bullet bul in bulletList) {
			Point bulPoint = { bul.get_x () - level.view.x , bul.get_y () - level.view.y };
			SDLRenderer.draw_image (ImageManager.bullet, bulPoint, true);
		}
	}
	
	// overriden id getter
	public override int get_id () {
		return 1;
	}
}
