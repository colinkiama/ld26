// TODO HELP SCREEN NEEDS MOUSE PICTURE, ANIMATION
// TODO stage change animation
// TODO Make things resolution dependent instead of hard coded
// TODO optimize drawing by only using BMPs not PNGs
// TODO HACK: load_png_alpha does NOT convert to screen pixel format
// TODO money stage optimizations
// TODO get rid of fill color on Stage


using SDL;
using SDLImage;
using Xml;
using Menu;

/**
 * My game for Ludum Dare 26. This is the main class that starts and controls
 * everything. Although it wasn't done in time. But whatever.
 *
 * Coded by Black Forest Programming (blackforestprogramming.tumblr.com)
 * 2013, protected under the Do What The Fuck You Want To Public Lisence (wtfpl.net)
 * Twitter : tastyorange
 * mdhitchens at gmail dot com 
 */
public class LD26 : Object {
	public unowned Screen screen; // the screen the player sees
	
	// screen properties
	public const int SCREEN_WIDTH = 640;
	public const int SCREEN_HEIGHT = 480;
	public const int SCREEN_BPP = 32;
	public const int SCREEN_FLAGS = SurfaceFlag.SWSURFACE;
	
	/** The actual game of this game **/
	public GameStage actualGame;
	/** The main menu screen **/
	public MainMenu menu;
	/** The help screen **/
	public HelpStage help;
	/** The stage which shows how much money you made **/
	public MoneyStage money;
	
	private Stage currentStage; // what stage we are currently updating and rendering
	
	private bool running = false; // whether the main loop is running or not
	
	private uint32 maxFPS = 50; // want only 50 frames per second
	private uint32 fps = 0; // the ACTUAL frames per second
	private uint32 timerDelay = 10; // how much time to waste if already performed the tick
	
	private bool doScreenshot = false; // whether a screenshot was requested
	
	/**
	 * Initializes the game and starts the main game loop.
	 */
	public LD26 () {
		screen = Screen.set_video_mode (SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SCREEN_FLAGS);
		assert (screen != null); // lazy way of making sure screen is made
		
		ImageManager.init (screen.format);
		SDLRenderer.init (screen);
		InputHandler.init ();
		PlayerVariables.init ();
		
		ImageManager.load_images ();
		
		menu = new MainMenu (this);
		actualGame = new GameStage (this);
		help = new HelpStage (this);
		money = new MoneyStage (this);
		currentStage = menu;
		
		start ();
	}
	
	/**
	 * Starts the main loop if it is not started.
	 */
	public void start () {
		if (running) return; // already running; stop
		running = true;
		run ();
	}
	
	/**
	 * Stops the main loop, thus terminating the game.
	 */
	public void quit () {
		running = false;
	}
	
	/**
	 * Tells the game to take a screenshot when it can.
	 */
	public void request_screenshot () {
		doScreenshot = true;
	}
	
	/**
	 * Takes a screenshot. Should not be called directly; set doScreenshot to
	 * true to request one to be taken.
	 */
	private void take_screenshot () {
		stdout.printf ("Saving screenshot to `screenshot.bmp'\n"); // TODO DEBUG
		var file = new RWops.from_file ("screenshot.bmp", "wb");
		screen.save (file);
		stdout.printf ("%s\n", SDL.get_error ());
		doScreenshot = false;
	}
	
	/**
	 * Changes the current stage to the given stage.
	 *
	 * @param stage The Stage to change to.
	 */
	public void change_stage (Stage stage) {
		currentStage = stage;
	}
	
	/**
	 * The main game loop. Updates and renders while restricting frames per
	 * second.
	 */
	private void run () {
		// all uint32 so that there are (hopefully) less issues
		uint32 interval = 1000 / maxFPS; // the length of a time stamp
		uint32 lastSecond = 0; // the last second which FPS was tabulated at
		uint32 ticks = 0; // how many ticks have happened this second
		
		while (running) {
			uint32 elapsed = SDL.Timer.get_ticks ();
			
			// if one second has passed calculate FPS
			if (elapsed >= lastSecond + 1000) {
				lastSecond += 1000;
				this.fps = ticks;
				ticks = 0;
				//stdout.printf ("FPS: %u\n", fps); // TODO DEBUG
			}
			
			// if we haven't updated in this time stamp
			if (elapsed >= lastSecond + interval * ticks) {
				InputHandler.process_input ();
				currentStage.update ();
				
				screen.fill (null, currentStage.fillColor); // fill the whole screen
				currentStage.render ();
				screen.flip (); // make sure the player can see what we drew
				
				if (doScreenshot) take_screenshot ();
				
				ticks++;
			} else {
				SDL.Timer.delay (timerDelay); // waste time until next tick
			}
		}
	}
	
	/**
	 * The entry point. Initializes SDL, creates the game, and after the game
	 * terminates quits SDL.
	 *
	 * @param args The commandline arguments passed in by the OS.
	 * @return The exit code to the OS.
	 */
	public static int main (string[] args) {
		Xml.Parser.init ();
		SDL.init ();
		SDLImage.init (InitFlags.PNG);
		
		new LD26 ();
		
		SDLImage.quit ();
		SDL.quit ();
		Xml.Parser.cleanup ();
		return 0;
	}
	
}
