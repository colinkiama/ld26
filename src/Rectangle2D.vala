// TODO set and get methods to lessen typing?
/**
 * A struct which represents a rectangle in two-dimensional Euclidean space
 * using computer screen coordinates.
 */
public struct Rectangle {
	/** The top left corner of the rectangle. **/
	public Point topLeft;
	/** The bottom right corner of the rectangle. **/
	public Point bottomRight;
}

/**
 * A wrapper for Rectangle, providing several methods for dealing with
 * rectangles.
 */
public class Rectangle2D {
	public Rectangle rectangle;
	
	/**
	 * Wrap the given Rectangle.
	 *
	 * @param rect The rectangle to wrap.
	 */
	public Rectangle2D (Rectangle rect) {
		rectangle = rect;
	}
	
	/**
	 * Creates and wraps a Rectangle given a point and dimensions.
	 *
	 * @param x The x location of the top left corner.
	 * @param y The y location of the top left corner.
	 * @param w The width of the rectangle.
	 * @param h The height of the rectangle.
	 */
	public Rectangle2D.from_dimensions (float x, float y, float w, float h) {
		Point topleft = { x, y };
		Point bottomright = { x + w, y + h };
		rectangle = { topleft, bottomright };
	}
	
	/**
	 * Determines whether this rectangle is touching the given rectangle.
	 * Essentially performs AABB collision checking.
	 *
	 * @param rect The rectangle to check collision with.
	 * @return true if the rectangles are colliding, false if not.
	 */
	public bool intersects_rectangle (Rectangle rect) {
		if (rectangle.bottomRight.x < rect.topLeft.x ||
			rectangle.bottomRight.y < rect.topLeft.y ||
			rectangle.topLeft.x > rect.bottomRight.x ||
			rectangle.topLeft.y > rect.bottomRight.y ) {
			return false;
		}
		return true;
	}
	
	/**
	 * Moves the rectangle, both the top left and bottom right corners, by the
	 * specified amount.
	 *
	 * @param deltaX The amount to move on the x-axis.
	 * @param deltaY The amount to move on the y-axis.
	 */
	public void move (float deltaX, float deltaY) {
		rectangle.topLeft.x += deltaX;
		rectangle.topLeft.y += deltaY;
		rectangle.bottomRight.x += deltaX;
		rectangle.bottomRight.y += deltaY;
	}
	
	/**
	 * Gets the width of this rectangle.
	 *
	 * @return The width of this rectangle.
	 */
	public float get_width () {
		return rectangle.bottomRight.x - rectangle.topLeft.x;
	}
	
	/**
	 * Gets the height of this rectangle.
	 *
	 * @return The height of this rectangle.
	 */
	public float get_height () {
		return rectangle.bottomRight.y - rectangle.topLeft.y;
	}
	
	/**
	 * Sets the new location for the rectangle.
	 *
	 * @param p The new point where this rectangle will be.
	 */
	public void set_location (Point p) {
		float w = get_width ();
		float h = get_height ();
		rectangle.topLeft = p;
		rectangle.bottomRight.x = p.x + w;
		rectangle.bottomRight.y = p.y + h;
	}
	
	/**
	 * Returns whether the given point is inside this rectangle.
	 *
	 * @param p The point to check.
	 * @return true if point is in this rectangle, false otherwise.
	 */
	public bool is_point_inside (Point p) {
		return (p.x >= rectangle.topLeft.x &&
		        p.y >= rectangle.topLeft.y &&
		        p.x <= rectangle.bottomRight.x &&
		        p.y <= rectangle.bottomRight.y);
	}
}
