using SDL;

/**
 * A class with only static methods and fields to deal with drawing things to
 * the screen.
 */
public class SDLRenderer : Object {
	private static unowned Screen screen = null; // the screen to draw things to
	
	/**
	 * Initializes the renderer. Must be called before any static methods from
	 * this class are called.
	 */
	public static void init (Screen scr) {
		screen = scr;
	}
	
	/**
	 * Draw the entire image to the screen at the given location.
	 *
	 * @param img The surface/image to draw
	 * @param p The point on the screen to draw the image at
	 * @param centered Whether to center the image around the given point.
	 */
	public static void draw_image (Surface img, Point p, bool centered = false) {
		if (centered) {
			Rect rect = { (int16)(p.x - img.w / 2), (int16)(p.y - img.h / 2), 0, 0 };
			img.blit (null, screen, rect);
		} else {
			Rect rect = { (int16)p.x, (int16)p.y, 0, 0 };
			img.blit (null, screen, rect);
		}
	}
	
	/**
	 * Draw a particular subimage from a SpriteSheet at a point on the screen,
	 * assuming subimages are laid out as a grid where each subimage can be
	 * accessed with a special x and y coordinate.
	 *
	 * @param ss The SpriteSheet to draw from.
	 * @param p The point on the screen to draw to.
	 * @param xsub The subimage's x spot.
	 * @param ysub The subimage's y spot.
	 */
	public static void draw_spritesheet (SpriteSheet ss, Point p, int xsub, int ysub) {
		Rect destrect = { (int16)p.x, (int16)p.y, 0, 0 };
		Rect srcrect = { (int16)(ss.subWidth * xsub), (int16)(ss.subHeight * ysub), (uint16)ss.subWidth, (uint16)ss.subHeight };
		ss.image.blit (srcrect, screen, destrect);
	}
	
	/**
	 * Draws a subimage from a SpriteSheet, but only from along the image's
	 * x-axis.
	 *
	 * @param ss The SpriteSheet to draw.
	 * @param p The point on the screen.
	 * @param xsub The subimage to draw.
	 * @param centered Whether to center the image around the point p.
	 */
	public static void draw_spritesheet_along_x (SpriteSheet ss, Point p, int xsub, bool centered = false) {
		if (centered) {
			Rect destrect = { (int16)(p.x - ss.subWidth / 2), (int16)(p.y - ss.subHeight / 2), 0, 0 };
			Rect srcrect = { (int16)(ss.subWidth * xsub), 0, (uint16)ss.subWidth, (uint16)ss.subHeight };
			ss.image.blit (srcrect, screen, destrect);
		} else {
			Rect destrect = { (int16)p.x, (int16)p.y, 0, 0 };
			Rect srcrect = { (int16)(ss.subWidth * xsub), 0, (uint16)ss.subWidth, (uint16)ss.subHeight };
			ss.image.blit (srcrect, screen, destrect);
		}
	}

}
