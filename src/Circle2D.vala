/**
 * A struct representing a circle  centered at a point.
 */
public struct Circle {
	/** The radius of the circle **/
	public float radius;
	/** The point the circle is centered at **/
	public Point center;
}

/**
 * A wrapper for the Circle struct which provides various helper methods.
 */
public class Circle2D : Object {
	public Circle circle;
	
	/**
	 * Create a new wrapper around the given circle.
	 *
	 * @param circ The circle to be wrapped.
	 */
	public Circle2D (Circle circ) {
		circle = circ;
	}
	
	/**
	 * Returns the radius of the wrapped circle.
	 *
	 * @return this.circle.radius
	 */
	public float get_radius () {
		return circle.radius;
	}
	
	/**
	 * Returns the x component of the point this circle is centered about.
	 *
	 * @return this.circle.center.x
	 */
	public float get_x () {
		return circle.center.x;
	}
	
	/**
	 * Returns the y component of the point this circle is centered about.
	 *
	 * @return this.circle.center.y
	 */
	public float get_y () {
		return circle.center.y;
	}
}
