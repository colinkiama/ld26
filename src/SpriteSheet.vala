// TODO should subWidth and subHeight be private?

using SDL;

/**
 * A wrapper for SDL_Surface which allows for multiple images on one Surface
 * and provides ways of selective blitting.
 */
public class SpriteSheet : Object {
	/** The SDL_Surface we are wrapping. **/
	public Surface image;
	/** The width of an individual subimage. **/
	public int subWidth;
	/** The height of an individual subimage. **/
	public int subHeight;
	
	/**
	 * Create SpriteSheet, expliciting providing all necessary paramaters.
	 *
	 * @param dir The image to load
	 * @param subw The width of any given subimage.
	 * @Param subh The height of any given subimage.
	 */
	public SpriteSheet (string dir, int subw, int subh) {
		if (dir.index_of (".png") != -1) {
			image = ImageManager.load_png_alpha (dir); // prevents valac complaining about ownership
		} else {
			image = ImageManager.load (dir);
		}
		subWidth = subw;
		subHeight = subh;
	}

	/**
	 * Create a SpriteSheet where all the subimages are along the x-axis of the
	 * image.
	 *
	 * @param dir The directory of the image to load.
	 * @param subw The width of any given subimage.
	 */
	public SpriteSheet.along_x (string dir, int subw) {
		if (dir.index_of (".png") != -1) {
			image = ImageManager.load_png_alpha (dir); // prevents valac complaining about ownership
		} else {
			image = ImageManager.load (dir);
		}
		subWidth = subw;
		subHeight = image.h;
	}
}
