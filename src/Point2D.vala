/**
 * A simple struct to keep track of a point in two dimensional Euclidean space
 * based on computer screen coordinates.
 */
public struct Point {
	/** The X component of the point **/
	public float x;
	/** The Y component of the point **/
	public float y;
}

/**
 * A class wrapper for Point which offers various helper methods.
 */
public class Point2D : Object {
	/** The point this class wraps around. **/
	public Point point;
	
	/**
	 * Wrap the given point with this class.
	 *
	 * @param p The point to wrap.
	 */
	public Point2D (Point p) {
		point = p;
	}
	
	/**
	 * Gets the x component of the wrapped point.
	 *
	 * @return this.point.x
	 */
	public float get_x () {
		return point.x;
	}
	
	/**
	 * Gets the y component of the wrapped point.
	 *
	 * @return this.point.y
	 */
	public float get_y () {
		return point.y;
	}
	
	/**
	 * Determines whether this point is inside the given rectangle.
	 *
	 * @param rect The rectangle to check whether this is inside of.
	 * @return true if this point is inside the rectangle, false if not.
	 */
	public bool is_inside_rectangle (Rectangle rect) {
		return (point.x >= rect.topLeft.x &&
		        point.y >= rect.topLeft.y &&
		        point.x <= rect.bottomRight.x &&
		        point.y <= rect.bottomRight.y);
	}
}
