/**
 * The extremely lazy way of setting up global player variables.
 */
public class PlayerVariables : Object {
	/** how many missions, and true if complete **/
	public static bool[] missionComplete;
	/** how much health the player has **/
	public static float health = 100;
	/** hoe much money the player has **/
	public static int money = 10;
	
	
	// prevent instantiation
	private PlayerVariables () {}
	
	/**
	 * Initializes the player variables. Must be called before any other method
	 * in this class.
	 */
	public static void init () {
		missionComplete = new bool[10];
		for (int i = 0; i < missionComplete.length; i++) {
			missionComplete[i] = false;
		}
		
		tryToLoadPlayer ();
	}
	
	// checks if there is a save game and attemps to load it
	private static void tryToLoadPlayer () {
		var stream = FileStream.open ("save.sav", "r");
		if (stream != null) { // if there is a same game
			string line;
			
			// load missions
			for (int i = 0; i < missionComplete.length; i++) {
				line = stream.read_line ();
				missionComplete[i] = bool.parse (line);
			}
			
			// load money
			line = stream.read_line ();
			money = int.parse (line);
			
			// TODO put other loading shtuff herez!
		}
	}
}
