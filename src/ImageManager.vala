
using SDL;
using SDLImage;

/**
 * A class with only static methods and fields to deal with loading and storing
 * image data.
 */
public class ImageManager : Object {
	/** A white anti-aliased circle **/
	public static Surface circle = null;
	/** The game logo **/
	public static Surface menuLogo = null;
	/** The large play arrow on the main menu **/
	public static SpriteSheet menuPlay = null;
	/** The large stop square on the main menu **/
	public static SpriteSheet menuStop = null;
	/** The large question mark on the main menu **/
	public static SpriteSheet menuHelp = null;
	/** The static tileset for a level **/
	public static SpriteSheet tileset = null;
	/** The player's bullet image **/
	public static Surface bullet = null;
	/** The large number used on various screens **/
	public static SpriteSheet bigNumbers = null;
	/** The plus sign on the money display screen **/
	public static Surface bigPlus = null;
	
	private static string baseDirectory = ""; // where the images are generally located
	private static uint32 colorKey = 0; // the color to use when color keying
	private static unowned PixelFormat screenFormat = null; // used for image optimization
	
	/**
	 * Initializes the image loader. Must be done before load_images is called.
	 *
	 * @param format The pixel format of the screen used for optimizing the loaded images.
	 */
	public static void init (PixelFormat format) {
		baseDirectory = "images/";
		screenFormat = format;
		colorKey = format.map_rgb (255, 0, 255);
	}
	
	/**
	 * Try to load all the images and optimize them for the screen we are
	 * drawing to. Assertions are built in so that if one image doesn't load
	 * then the game will halt.
	 */
	public static void load_images () {
		circle = load_png_alpha ("circle.png");
		menuLogo = load_png_alpha ("menu-logo.png");
		menuPlay = new SpriteSheet.along_x ("menu-play-strip-smaller.png", 96);
		menuStop = new SpriteSheet.along_x ("menu-stop-strip-smaller.png", 96);
		menuHelp = new SpriteSheet.along_x ("menu-help-strip-smaller.png", 96);
		tileset = new SpriteSheet ("tileset000.bmp", 32, 32);
		bullet = load_png_alpha ("bullet.png");
		bigNumbers = new SpriteSheet.along_x ("big-numbers.png", 91);
		bigPlus = load_png_alpha ("plus.png");
	}
	
	/** Tries to load an image. **/
	public static Surface load (string dir) {
		var file = new RWops.from_file (baseDirectory + dir, "rb"); // open the file
		var img = new Surface.load (file); // load the file
		assert (img != null); // lazy way out :P
		
		var opt = img.convert (screenFormat, 0); // optimize the loaded image
		assert (opt != null); // again, moar laziness
		
		opt.set_colorkey (SurfaceFlag.SRCCOLORKEY, colorKey); // set its color key
		return opt; // return the good, optimized image
	}
	
	/** Tries to load an image. **/
	public static Surface load_png_alpha (string dir) {
		var file = new RWops.from_file (baseDirectory + dir, "rb"); // open the file
		var img = SDLImage.load_png (file); // load the file
		assert (img != null); // lazy way out :P
		
		// TODO HACK: not converting to screen format!
		
		return img; // return the good, optimized image
	}
}
