/**
 * Represents a seperate screen with it's own logic, entities, and render cycle.
 * Should be inherited to make use of.
 */
public abstract class Stage : Object {
	/** The game that created the stage **/
	public weak LD26 parent;
	/** What color to fill the background with **/
	public uint32 fillColor = 0;
	
	/**
	 * Create a new stage with the specified game parent.
	 *
	 * @param parent The game this was created from.
	 */
	public Stage (LD26 game) {
		parent = game;
	}

	/**
	 * Draws one frame of the game to the screen. All drawing operations should
	 * be done here.
	 */
	public abstract void render ();
	
	/**
	 * Perform one frame worth of game logic. Most, if not all, computation
	 * should go here.
	 */
	public abstract void update ();
	
	/**
	 * Every stage should have a unique ID so that they can be distinguished.
	 *
	 * @return The unique ID of this stage.
	 */
	public abstract int get_id ();
}
