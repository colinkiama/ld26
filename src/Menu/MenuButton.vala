// TODO update is messy!

namespace Menu {
	/**
	 * Represents the signature of a method which is called when a MenuButton is
	 * pressed.
	 */
	public delegate void ActionMethod ();

	/**
	 * A button on the main menu which interacts with the mouse.
	 */
	public class MenuButton : Object {
		/** The collision mask for the button **/
		public Rectangle mask;
		/** The location of the center of the button **/
		public Point center;
		/** The SpriteSheet to draw this button with **/
		public weak SpriteSheet image;
	
		/** The current animation frame **/
		public float frame = 0;
		/** The speed at which the animation progresses **/
		public float speed = 0;
	
		public weak MainMenu parent; // the menu that created this button
		private static Point bounds = { 96, 96 }; // height and width of each button

		private unowned ActionMethod action;
	
		/**
		 * Creates a new button centered at the given position.
		 *
		 * @param menu The Menu to which this button belongs.
		 * @param p The spot where the button is centered.
		 * @param img The SpriteSheet to use in order to draw the button.
		 * @param act The method that is called when the button is clicked.
		 */
		public MenuButton (MainMenu menu, Point p, SpriteSheet img, ActionMethod act) {
			Point p1 = { p.x - bounds.x / 2, p.y - bounds.y / 2 };
			Point p2 = { p.x + bounds.x / 2, p.y + bounds.y / 2 };
			mask = { p1, p2 };
			parent = menu;
			center = p;
			image = img;
			action = act;
		}
	
		/**
		 * Checks whether the mouse is inside the button and reacts accordingly.
		 */
		public void update () {
			if (InputHandler.mouseSpot.is_inside_rectangle (mask)) {
				speed = 1;
				if (InputHandler.left_mouse_pressed) {
					InputHandler.left_mouse_pressed = false;
					speed = 0;
					frame = 0;
					action ();
				}
			
			} else {
				speed = -1;
			}
		
			frame += speed;
			if (frame <= 0) {
				speed = 0;
				frame = 0;
			} else if (frame >= 7) {
				speed = 0;
				frame = 7;
			}
		}
	
		/**
		 * Draw the button
		 */
		public void draw () {
			SDLRenderer.draw_spritesheet_along_x (image, center, (int)frame, true);
		}
	}
}
