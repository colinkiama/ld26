namespace Menu {
	/**
	 * Shows the money you got at the end of the level with a fancy animation
	 * (hopefully)
	 */
	public class MoneyStage : Stage {
		private int total = 13025; // the total to show
	
		// override the constructor
		public MoneyStage (LD26 game) {
			base (game);
			fillColor = game.screen.format.map_rgb (0, 128, 255);
		}
	
		/** A function to draw the numbers. **/
		private void draw_total (Point p) {
			string str = "$" + total.to_string ();
			int width = ImageManager.bigNumbers.subWidth;
		
			for (int i = 0; i < str.length; i++) {
				unichar toDraw = str.get_char (i);
			
				int index;
				if (toDraw == '$') {
					index = 0;
				} else {
					index = str[i] - '0' + 1;
				}
			
				SDLRenderer.draw_spritesheet_along_x (ImageManager.bigNumbers, {p.x + i * width, p.y}, index, true);
			}
		}
	
		// override the update moethod
		public override void update () {
			if (InputHandler.hit_close) parent.quit ();
		}
	
		// override the render method
		public override void render () {
			draw_total ({80, 240});
		}
	
		// override id getter
		public override int get_id () {
			return 3;
		}
	}
}
