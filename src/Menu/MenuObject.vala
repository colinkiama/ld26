// TODO should parent be public?
// TODO is speed necessary?

namespace Menu {
	/**
	 * A class which represents the framework for an object which appears on a
	 * menu screen.
	 */
	public abstract class MenuObject : Object {
		/** The collision mask for the object **/
		public Rectangle mask;
		/** The location of the center of the object **/
		public Point center;
		/** The SpriteSheet to draw this object with **/
		public weak SpriteSheet image;
		
		/** The current animation frame **/
		public float frame = 0;
		/** The speed at which the animation progresses **/
		public float speed = 0; 
		
		/** The menu that created this object **/
		public weak Menu parent;
		
		/**
		 * Create a MenuObject given the parent menu, an image, and location.
		 * The collision mask is determined from the size of a subimage of the
		 * given sprite sheet.
		 *
		 * @param menu The Menu to which this belongs.
		 * @param p The point around which the object is centered.
		 * @param img The sprite sheet to use for drawing.
		 */
		public MenuObject (Menu menu, Point p, SpriteSheet img) {
			parent = menu;
			center = p;
			image = img;
			
			int imgw = img.subWidth;
			int imgh = img.subHeight;
			
			Point tl = { p.x - imgw / 2 , p.y - imgh / 2 };
			Point br = { p.x + imgw / 2 , p.y + imgh / 2 };
			mask = { tl , br };
		}
		
		/**
		 * Checks whether the mouse is inside the button and reacts accordingly.
		 */
		public void update () {
			if (InputHandler.mouseSpot.is_inside_rectangle (mask)) {
				hover ();
				
				if (InputHandler.left_mouse_pressed) {
					InputHandler.left_mouse_pressed = false;
					click ();
				}
			} else {
				unhover ();
			}
		}
		
		/**
		 * Draw the object from the sprite sheet using the frame and position.
		 */
		public void draw () {
			SDLRenderer.draw_spritesheet_along_x (image, center, (int)frame, true);
		}
		
		/**
		 * What to do when the mouse is inside the collision mask of the object.
		 */
		public abstract void hover ();
		
		/**
		 * What to do when the mouse is outside the collision mask of the
		 * object.
		 */
		public abstract void unhover ();
		
		/**
		 * What to do when the button is clicked.
		 */
		public abstract void click ();
	}
}
