// TODO where does Stage come from?
// TODO where does LD26 come from?
// TODO transition system
// TODO what about get_id ()>?

namespace Menu {
	/**
	 * Defines common menu behavior. Includes a list of objects which one can
	 * interact with on the menu.
	 */
	public abstract class Menu : Stage {
		/** List of all the objects automatically handled by the menu **/
		public SList<MenuObject> objects = new SList<MenuObject> ();
		
		/**
		 * Create a new menu attached to a game.
		 *
		 * @param game The game which controls the menu
		 */
		public Menu (LD26 game) {
			base (game);
		}
		
		/**
		 * Override Stage's update to update the menu objects.
		 */
		public override void update () {
			foreach (MenuObject obj in objects) {
				obj.update ();
			}
		}
		
		/**
		 * Override Stage's render to draw the menu objects to the screen.
		 */
		public override void render () {
			foreach (MenuObject obj in objects) {
				obj.draw ();
			}
		}
		
		/**
		 * Add an object to the menu object list.
		 *
		 * @param obj The object to add.
		 */
		public void add (MenuObject obj) {
			objects.append (obj);
		}
	}
}
