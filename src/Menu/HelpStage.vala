namespace Menu {
	/**
	 * The screen that tells how to play the game.
	 */
	public class HelpStage : Menu {
		/**
		 * Create the help screen given the game who created it.
		 *
		 * @param game The game who made this stage.
		 */
		public HelpStage (LD26 game) {
			base (game);
			fillColor = game.screen.format.map_rgb (0, 128, 255);
		}
	
		// overriden update method
		public override void update () {
			base.update ();	
			
			if (InputHandler.hit_close) parent.quit ();
			if (InputHandler.space_pressed) {
				parent.request_screenshot ();
				InputHandler.space_pressed = false;
			}
			if (InputHandler.left_mouse_pressed) {
				InputHandler.left_mouse_pressed = false;
				parent.change_stage (parent.menu);
			}
		
		}
	
		// overriden render method
		public override void render () {
			base.render ();
			
			SDLRenderer.draw_spritesheet_along_x (ImageManager.menuHelp, { 320 , 240 }, 0, true);
		}
	
		// overriden id getter
		public override int get_id () {
			return 2;
		}
	}
	
	// contained in the code right from the project in Dropbox
	// causes compile errors so commented out
	//class TestObject : MenuObject {
	//	
	//}
}
