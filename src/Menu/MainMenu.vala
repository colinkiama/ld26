using SDL; // not needed, but just in case

namespace Menu {
	/**
	 * Takes care of the main menu, including updating, rendering, and responding
	 * to user input.
	 */
	public class MainMenu : Stage {
	
		private MenuButton playButton; // play
		private MenuButton stopButton; // stop
		private MenuButton helpButton; // help
	
		/**
		 * Create the menu screen given the game who created it.
		 *
		 * @param game The game who made this stage.
		 */
		public MainMenu (LD26 game) {
			base (game);
			fillColor = game.screen.format.map_rgb (0, 128, 255);
		
			playButton = new MenuButton (this, { 320 - 160, 320 }, ImageManager.menuPlay, play_press);
			stopButton = new MenuButton (this, { 320 + 160, 320 }, ImageManager.menuStop, stop_press);
			helpButton = new MenuButton (this, { 320, 320 }, ImageManager.menuHelp, help_press);
		}
	
		/**
		 * The action performed when the play button is pressed.
		 */
		public void play_press () {
			parent.change_stage (parent.actualGame);
		}
	
		/**
		 * The action performed when the stop button is pressed.
		 */
		public void stop_press () {
			parent.quit ();
		}
	
		/**
		 * The action performed when the help button is pressed.
		 */
		public void help_press () {
			parent.change_stage (parent.help);
		}
	
		// overriden update method
		public override void update () {
			if (InputHandler.hit_close) parent.quit ();
			if (InputHandler.space_pressed) {
				parent.request_screenshot ();
				InputHandler.space_pressed = false;
			}
			if (InputHandler.enter_pressed) {
				InputHandler.enter_pressed = false;
				parent.change_stage (parent.actualGame);
			}
			playButton.update ();
			stopButton.update ();
			helpButton.update ();
		}
	
		// overriden render method
		public override void render () {
			SDLRenderer.draw_image (ImageManager.menuLogo, { 320, 96 }, true);
			playButton.draw ();
			stopButton.draw ();
			helpButton.draw ();
		}
	
		// overriden id getter
		public override int get_id () {
			return 0;
		}
	}
}
