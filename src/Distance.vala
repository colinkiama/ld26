using GLib.Math;

/**
 * Methods for determining various types of distances.
 */
public class Distance : Object {
	/**
	 * Determines the Euclidean distance between two Points, that is, the
	 * shortest possible path in Euclidean space between two points.
	 *
	 * @param a One point
	 * @param b Another point
	 * @return The Euclidean distance between a and b
	 */
	public static float euclidean (Point a, Point b) {
		float c = b.x - a.x;
		float d = b.y - a.y;
		return sqrtf (c * c + d * d);
	}
	
	/**
	 * Determines the Manhatten distance between two points, that is, the sum
	 * of the absolute value of the difference of the components of two points.
	 *
	 * @param a One point.
	 * @param b Another point.
	 * @return The Manhatten distance between a and b
	 */
	public static float manhatten (Point a, Point b) {
		return fabsf (b.x - a.x) + fabsf (b.y - a.y);
	}
}
