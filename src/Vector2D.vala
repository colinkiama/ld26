using GLib.Math;

/**
 * Another wrapper for Point, this time with methods more suited for vectors in
 * two dimensional Euclidean space based on screen coordinates.
 */
public class Vector2D : Point2D {
	
	/**
	 * Create a new Vector2D wrapper for the given point.
	 *
	 * @param p The Point we are wrapping.
	 */
	public Vector2D (Point p) {
		base (p);
	}
	
	/**
	 * Calculates the length, or magnitude, of the vector.
	 *
	 * @return The length/magnitude of this vector.
	 */
	public float magnitude () {
		return sqrtf (point.x * point.x + point.y * point.y);
	}
	
	/**
	 * Calculates the angle between this vector and the x axis.
	 *
	 * @return The angle, in radians, between this vector and the x-axis.
	 */
	public float direction () {
		return atan2f (-point.y, -point.x);
	}
}
