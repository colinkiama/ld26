// TODO make width and height private
// TODO more things need to be private

/**
 * A level from the game.
 */
public class Level : Object {
	/** The width of the level in tiles. **/
	public int width;
	/** The height of the level in tiles. **/
	public int height;
	/** The tile data. **/
	public uchar[,] tiles; // 0 is no tile, 1 is first on tileset, 2 is second on tileset, ...
	/** The location of the view in the level, in pixels **/
	public Point view;
	/** how much money the player has found this level **/
	public int moneyFound = 0;
	
	/** How much of the level we see, in tiles **/
	public static Point viewport = { 20, 15 };
	/** Where the levels are **/
	public static string base_directory = "levels/";
	
	/** A singly linked list of all the walls **/
	public SList<Rectangle2D> wallList = new SList<Rectangle2D> ();
	
	/**
	 * Create a blank level with the specified dimensions.
	 * @param w The width of the level.
	 * @param h The height of the level.
	 */
	public Level (int w, int h) {
		width = w;
		height = h;
		tiles = new uchar[width, height];
		view = { 0, 0 };
	}
	
	/**
	 * Loads a level from a specially formated file.
	 *
	 * @param dir The directory relative to base_directory to load from
	 */
	public Level.from_file (string dir) {
		view = { 0, 0 };
		
		var stream = FileStream.open (base_directory + dir, "r");
		assert (stream != null); // lazy error checking
		
		// read width and height
		string line = stream.read_line ();
		string[] param = line.split (",");
		width = int.parse (param[0]);
		height = int.parse (param[0]);
		
		tiles = new uchar[width, height];
		
		// read tile data
		for (int y = 0; y < height; y++) {
			line = stream.read_line ();
			param = line.split (",");
			
			for (int x = 0; x < width; x++) {
				tiles[x,y] = (uchar)int.parse (param[x]);
			}
		}
		
		// read objects
		while ((line = stream.read_line ()) != null) {
			param = line.split (",");
			int id = int.parse (param[0]);
			
			switch (id) {
				case 0: // wall
					stdout.printf ("Making rectangle\n"); // TODO debug
					wallList.append (new Rectangle2D.from_dimensions (int.parse (param[1]), int.parse (param[2]), int.parse (param[3]), int.parse (param[4])));
					break;
			}
		}
	}
	
	/**
	 * Centers the view on the given point.
	 *
	 * @param p The point on which the view is centered.
	 */
	public void center_view (Point p) {
		view.x = p.x - (viewport.x * get_tile_width ()) / 2;
		view.y = p.y - (viewport.y * get_tile_height ()) / 2;
	}
	
	/**
	 * Returns how wide a tile is in pixels, determined by the tileset.
	 *
	 * @return The width of any tile in pixels.
	 */
	public int get_tile_width () {
		return ImageManager.tileset.subWidth;
	}
	
	/**
	 * Returns how tall a tile is in pixels, determined by the tileset.
	 *
	 * @return The height of any tile in pixels.
	 */
	public int get_tile_height () {
		return ImageManager.tileset.subHeight;
	}
	
	/**
	 * Returns the width dimension of the level in tiles.
	 *
	 * @return The width of the level in tiles.
	 */
	public int get_width () {
		return width;
	}
	
	/**
	 * Returns the height dimension of the level in tiles.
	 *
	 * @return The height of the level in tiles.
	 */
	public int get_height () {
		return height;
	}
	
	/**
	 * Draws the level with the already set view and viewport using the loaded
	 * tileset.
	 */
	public void draw () {
		int gridW = ImageManager.tileset.subWidth;
		int gridH = ImageManager.tileset.subHeight;
		
		// for a rectangle with top left of view and bottom left of view + viewport
		
		for (int y = (int)(view.y / gridH) ; y < (int)(view.y / gridH + viewport.y + 1); y++) { // draw one more so it fills screen
			for (int x = (int)(view.x / gridW); x < (int)(view.x / gridW + viewport.x + 1); x++) {  // draw one more so it fills screen
				if (x < 0 || y < 0 || x >= width || y >= height) continue;
				if (tiles[x,y] == 0) continue;
				
				int a = (tiles[x,y] - 1) % 8;
				int b = (tiles[x,y] - 1) / 8;
				
				SDLRenderer.draw_spritesheet (ImageManager.tileset, { x * gridW - view.x , y * gridH - view.y }, a, b);
			}
		}
	}
}
