using GLib.Math;

/**
 * Holds all the player variables. Inherits Point2D because.
 */
public class Player : Point2D {
	/** The collision mask for the player **/
	public Rectangle2D mask;
	private Point prev; // the last place the player was.
	
	/**
	 * Make a new player at the desired spot.
	 *
	 * @param p The point to place the player.
	 */
	public Player (Point p) {
		base (p);
		prev = p;
		mask = new Rectangle2D.from_dimensions (p.x - 14, p.y - 14, 28, 28);
	}
	
	/**
	 * Move the player.
	 *
	 * @param v The vector which represents the player movement.
	 */
	public void move (Vector2D v) {
		prev.x = this.get_x ();
		prev.y = this.get_y ();
	
		float dir = v.direction ();
		float mag = ((v.magnitude () - 32) / 100.0f).clamp (0,2);
		float movex = cosf (dir) * mag;
		float movey = sinf (dir) * mag;
		this.point.x += movex;
		this.point.y += movey;
		mask.set_location ({this.point.x - 14, this.point.y - 14});
	}
	
	/**
	 * Makes the player go to his previous location.
	 */
	public void goto_previous_spot () {
		this.point = prev;
		mask.set_location ({this.point.x - 14, this.point.y - 14});
	}
}
