using Xml;

/**
 * Functions for loading Tiled levels using libxml2.
 */
namespace TiledLoader {
	/** All loading is done relative to this directory. **/
	public const string base_directory = "levels/";

	/**
	 * Load a level from a Tiled file.
	 *
	 * @param dir The file to load, relative to the base directory.
	 */
	public Level load_tiled (string dir) {
		Xml.Doc* doc = Xml.Parser.parse_file (base_directory + dir);
		assert (doc != null); // lazy error checking
		
		Xml.Node* root = doc->get_root_element ();
		assert (root != null); // sigh lazy checking
		
		// the root tag, <map> stores width and height
		int width = 0;
		int height = 0;
		get_width_height (root, ref width, ref height);
		
		Level lev = new Level (width, height);
		parse_nodes (root, lev); // start going through nodes!
		
		delete doc; // done parsing, free memory
		
		return lev;
	}
	
	/** Goes through all the nodes, figures out what to do with them, modifies the given level **/
	private void parse_nodes (Xml.Node* node, Level lev) {
		for (Xml.Node* iter = node->children; iter != null; iter = iter->next) { // for each child of node
            if (iter->type != ElementType.ELEMENT_NODE) continue; // Spaces between tags are also nodes, discard them

            string node_name = iter->name;	// Get the node's name
            
            // if found <data> assume it is background and load as such 
            if (node_name == "data") {
  	            string node_content = iter->get_content ();	// Get the node's content with <tags> stripped?
  	            parse_data (node_content, lev);
            }
            
            // if tag is <object> assume it is a wall and load as such
            if (node_name == "object") {
            	Rectangle2D rect = make_rectangle (iter);
            	lev.wallList.append (rect);
            }

            // recursion; parse the children
            parse_nodes (iter, lev);
        }
	}
	
	/** Given a <map> tag extract the width and height attributes **/
	private void get_width_height (Xml.Node* node, ref int w, ref int h) {
		assert (node->name == "map"); // lazy error checking
		
		for (Xml.Attr* prop = node->properties; prop != null; prop = prop->next) { // for each property
            string attr_name = prop->name;

			if (attr_name == "width") w = int.parse (prop->children->content);
			if (attr_name == "height") h = int.parse (prop->children->content);
        }
    }
    
    /** Given the CSV data parse it and put it into the given level. **/
    private void parse_data (string data, Level lev) {
    	assert (lev != null); // lazy error checking
    	
    	string[] values = data.split (","); // CSV so seperate by commas ;)
    	int index = 0;
    	
    	// process the data
    	foreach (string thing in values) {
    		if (data == "\n") continue; // ignore new line characters
    		
    		int x = index % lev.get_width ();
    		int y = index / lev.get_height ();
    		
    		lev.tiles[x, y] = (uchar)int.parse (values[index]);
    		
    		index++;
    	}
    }
    
    /** Given a node which represents a wall object, use the attributes to make a rectangle **/
    private Rectangle2D make_rectangle (Xml.Node *node) {
    	float x = 0;
    	float y = 0;
    	float w = 0;
    	float h = 0;
    	
    	for (Xml.Attr* prop = node->properties; prop != null; prop = prop->next) { // for each property
            string attr_name = prop->name;

			if (attr_name == "x") x = int.parse (prop->children->content);
			if (attr_name == "y") y = int.parse (prop->children->content);
			if (attr_name == "width") w = int.parse (prop->children->content);
			if (attr_name == "height") h = int.parse (prop->children->content);
        }
        
        return new Rectangle2D.from_dimensions (x, y, w, h);
	}    
    
}
